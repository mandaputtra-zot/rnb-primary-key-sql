module.exports =  {
  INCREMENT_TABLE: 'increment_table',
  UUID_TABLE: 'uuid_table',
  UUID_AND_INCREMENT_TABLE: 'uuid_and_increment',
  TIMESTAMP_UNIX_TABLE: 'timestamp_unix',
  ULID_TABLE: 'ulid_table'
}
