const { 
  INCREMENT_TABLE, 
  TIMESTAMP_UNIX_TABLE, 
  UUID_AND_INCREMENT_TABLE, 
  UUID_TABLE, 
  ULID_TABLE 
} = require('./utils')

const db = require('./db')

async function createTable() {
  // Create table for testing
  try {
    // UUID_TABLE
    await db.query(`
      CREATE EXTENSION IF NOT EXISTS pgcrypto;

      CREATE TABLE IF NOT EXISTS ${UUID_TABLE} (
        id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
        name VARCHAR (255) NOT NULL
      )
    `)

    // INCREMENT_TABLE
    await db.query(`
      CREATE TABLE IF NOT EXISTS ${INCREMENT_TABLE} (
        id SERIAL PRIMARY KEY UNIQUE NOT NULL,
        name VARCHAR (255) NOT NULL
      )
    `)

    // INCREMENT_UNIX_TIMESTAMP
    await db.query(`
      CREATE TABLE IF NOT EXISTS ${TIMESTAMP_UNIX_TABLE} (
        id int8 PRIMARY KEY UNIQUE NOT NULL,
        name VARCHAR (255) NOT NULL
      )
    `)

    // INCREMENT_WITH_UUID / OR WITH SLUG
    await db.query(`
      CREATE TABLE IF NOT EXISTS ${UUID_AND_INCREMENT_TABLE} (
        id SERIAL PRIMARY KEY NOT NULL,
        uuid VARCHAR (255) UNIQUE NOT NULL,
        name VARCHAR (255) NOT NULL
      )
    `)

    // INCREMENT_WITH_UUID / OR WITH SLUG
    await db.query(`
      CREATE TABLE IF NOT EXISTS ${ULID_TABLE} (
        id VARCHAR (255) PRIMARY KEY NOT NULL,
        name VARCHAR (255) NOT NULL
      )
    `)

    console.log('TABLE CREATION END')
  } catch (error) {
    throw error;
  }
}

module.exports = createTable
