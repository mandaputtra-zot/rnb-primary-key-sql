# rnb-primary-key-sql

Research and benchmark for SQL primary key generation, this repo tried to answer following question :

- Do Unix timestamps are safe to use as Primary Key?
- Do using string as primary key affect performance?
- Which I should be use in the future?

## How to run?

Environment variables, and the default values :

```
ENTROPY=1000 # random number from 1 - 1000
ROW_ADDED=1000 # how many row do you want to create
```

Running the benchmark:

```
# install dependencies
$ npm install

# run postgres on local as docker.
$ docker run --rm --name pg-docker -e POSTGRES_PASSWORD=postgres -d -p 5432:5432 -v $HOME/docker/volumes/postgres:/var/lib/postgresql/data postgres

$ node index.js # run with default env

$ ENTROPY=10000 ROW_ADDED=10000 node index.js # run with custom env
```

Remember, the benchmark are using naive n+1 function, so if you use 1 million ROW_ADDED, it will takes forever to complete.

## How do I benchmark?

After creating the table, the program will run insert to the table simultaneously using JavaScript `Promise.allSelted()` more info about it [here](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/allSettled)

## Result

Using default value as an env, I only use insert and count here, maybe using using `JOIN`, `SORT BY`, etc, will also help to understand the effect of using string or integer as primary key?
### INSERT INTO

| name               | perf     | errorLength | errorReason                                                          |
|--------------------|----------|-------------|----------------------------------------------------------------------|
| increment_table    | 238.93ms | 0           | no errors                                                            |
| timestamp_unix     | 595.15ms | 360         | duplicate key value violates unique constraint "timestamp_unix_pkey" |
| uuid_table         | 205.61ms | 0           | no errors                                                            |
| uuid_and_increment | 183.69ms | 0           | no errors                                                            |
| ulid_table         | 187.77ms | 0           | no errors                                                            |

### COUNT (*)

| name               | perf   | rows |
|--------------------|--------|------|
| increment_table    | 2.75ms | 1000 |
| timestamp_unix     | 1.15ms | 640  |
| uuid_table         | 0.95ms | 1000 |
| uuid_and_increment | 0.91ms | 1000 |
| ulid_table         | 0.80ms | 1000 |

## Conclusion

This are opinionated answer to the above question:

- Do Unix timestamps are safe to use as Primary Key? No, It will likely cause a collision
- Do using string as primary key affect performance? On this matter not really, the UUID table are more faster! Why? Find out below.
- Which I should be use in the future? It depends, I'll try to answer below

As always will be, give this conclusion a five minutes..

#### Using increment as primary key

This solution is almost perfect, It doesn't have any collision. But using primary id as an user facing app could be flawed.

| pros              | cons                                                                                                                                                  |
|-------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------|
| Easy to implement | Easy guess on certain item, you may not want to use `/api/user/:increment_id` some hacker, or maybe your competitor could easily scrap your user data |
|                   | It's impossible to create records concurrently because each insert has to wait in line to receive its id                                              |


#### Using Unix timestamps as primary key

For slow request per second this solution are safe enough, since running it with small ROW_ADDED wouldn't likely to cause collision, but on a service that must handling 1K/request per second it could be a trouble.

The error that is produces during benchmark are also why this approach are the slowest, when there are no collision errors it is on par with UUID as primary key.

For the record I also thought that it will only happen on Node.js, but no. I tested the algorithm I use on Clojure and Golang.

```js
// The algorithm, get unix time stamp and add random value
const timeStamp = Math.floor(new Date().getTime() / 1000 + Math.random() * (ENTROPY - 1) + 1);
```

Clojure and Node.js Unix timestamps length are 13, if you divide it by 1000 it would be 10, so it is the same. However on Golang `time.now().UnixNano` length are 19 but it is still the same, collision happen, try to run the code on this repo:

```bash
$ go run unix_timestamp.go # run it multiple times, you'll see that some numbers are same
```

| pros              | cons                      |
|-------------------|---------------------------|
| Not easy to guess | Likely to cause collision |

#### Using UUID as primary key

Perfect, using UUID v4 no collision happen, unless you generated 1 billion UUID v4 in a second, as wikipedia says.

You may notice that this is strange, using UUID as primary key are actually faster than the increment one, this is no magic. Using `SERIAL` / `AUTO_INCREMENT` on postgres actualy the same as running this query:

```sql
CREATE SEQUENCE table_name_id_seq;

CREATE TABLE table_name (
    id integer NOT NULL DEFAULT nextval('table_name_id_seq')
);

ALTER SEQUENCE table_name_id_seq
OWNED BY table_name.id;
```

It checks what are the table it runs 1 query, but 2 operations every time data inserted on the database.

| pros              | cons                                                  |
|-------------------|-------------------------------------------------------|
| Not easy to guess | Very random, not alphabetically sortable              |
|                   | Likely to slow on multiple join (not yet benchmarked) |


#### Using unique UUID and id as primary key

Perfect, no collision. This is the fast one and I still confuses why and how , since it had more column and it does 2 operation as auto increment. Maybe the databases doing some magic, I don't know for sure ~ but here ya go.

| pros                                                                     | cons                                                                                                      |
|--------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------|
| Not easy to guess since you will use UUID for your API `/api/user/:uuid` | You must remember that, UUID are for user facing api and use primary key id as system wide                |
| Easy to debug since you'll use autoincrement for your join table         | More bytes to store on the databases                                                                      |
|                                                                          | It's impossible to create records concurrently because each insert has to wait in line to receive its id. |
|                                                                          | Magically the fast one (it is cons because I don't know why)                                              |

### Using new kid on the block, ULID

Perfect no collision, sortable value by database, prettier than UUID.

| pros              | cons                                                  |
|-------------------|-------------------------------------------------------|
| Not easy to guess | likely to slow on multiple join (not yet benchmarked) |
| Pretty 😃          |                                                       |
|                   |                                                       |

#### My choice?

After some trial, running this benchmark again and again with different env, and reading some articles on how other companies used it.

I'll choose using new kid on the block ULID as primary key. It is safe, and when your ULID somehow broke (0.0000128% chance in the next 100 Years according to Zendesk research), you can still change it. If you're having a CRDT app, and had many clients and maybe storing the local user events on SQLite database on their phone, it unlikely caused clash with the main databases on your server if you choose to write the id with ULID on local client SQLite database.

ULID is new, but the benefit are proven by some big companies (Zendesk, etc) it should safe if you standing in the shoulder of a giant. Please read their finding on benchmarking ULID.

Instagram are also using timestamps as their primary key, but their approach are different, you could read their article below.

So, check the code, maybe you'll find a way to improve this benchmark, find some wrong approach on things that I wrote.

## Reference

Can't find some paper, lul. I use wikipedia and StackOverflow.

- https://stackoverflow.com/a/48583244/5672961
- https://en.wikipedia.org/wiki/Natural_key
- https://en.wikipedia.org/wiki/Surrogate_key
- https://stackoverflow.com/questions/1155008/how-unique-is-uuid
- https://en.wikipedia.org/wiki/Universally_unique_identifier#Version_4_(random)
- https://www.postgresqltutorial.com/postgresql-serial/
- https://medium.com/zendesk-engineering/how-probable-are-collisions-with-ulids-monotonic-option-d604d3ed2de
- https://instagram-engineering.com/sharding-ids-at-instagram-1cf5a71e5a5c
- https://www.honeybadger.io/blog/uuids-and-ulids/
- https://github.com/ulid/spec
- https://firebase.googleblog.com/2015/02/the-2120-ways-to-ensure-unique_68.html?m=1
