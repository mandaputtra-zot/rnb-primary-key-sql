package main

import (
	"fmt"
	"math/rand"
	"time"
)

func GenerateID() int64 {
	return time.Now().UnixNano() + int64(rand.Intn(10000))
}

func removeDuplicateInt(intSlice []int64) int {
	allKeys := make(map[int64]bool)
	list := []int64{}
	for _, item := range intSlice {
		if _, value := allKeys[item]; !value {
			allKeys[item] = true
			list = append(list, item)
		}
	}
	return len(list)
}

func main() {
	values := []int64{}

	for i := 0; i < 1000; i++ {
		values = append(values, GenerateID())
	}

  fmt.Printf("Lenght of value: %d \n", len(values))
	notDupLen := removeDuplicateInt(values)
  fmt.Printf("After removing duplicates: %d \n",notDupLen)
  fmt.Printf("Duplicates found: %d \n", len(values) - notDupLen)
}
