const create = require('./benchmark/create')
const count = require('./benchmark/count')

const dropTable = require('./drop_test_table')
const createTable = require('./create_test_table')
const db = require('./db')

;(async () => {
  try {
    console.log('----- TABLE SETUP')
    await dropTable()
    await createTable()
    console.log('----- TABLE SETUP FINISH')

    await create()
    await count()

    db.end()
    process.exit(1)
  } catch (error) {
    db.end()
    throw error
  }
})().catch(err => console.log(err.stack))