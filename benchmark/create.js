const db = require('../db')
const uuid = require('uuid')
const ulid = require('ulid')

const {
  performance
} = require('perf_hooks')
const {
  INCREMENT_TABLE,
  TIMESTAMP_UNIX_TABLE,
  UUID_AND_INCREMENT_TABLE,
  UUID_TABLE,
  ULID_TABLE
} = require('../utils')

// Create 1000 row, on every database
const ROW_ADDED = +process.env.ROW_ADDED || 1000
const ENTROPY = +process.env.ENTROPY || 1000

// Create 1000 array function
// Insert on increment table
async function increment() {

  let func = []
  for (let i = 0; i < ROW_ADDED; i++) {
    const qr = db.query(`INSERT INTO ${INCREMENT_TABLE} ( name ) VALUES ($1)`, ['name'])
    func.push(qr)
  }
  const start = performance.now()
  const result = await Promise.allSettled(func)
  const end = performance.now()

  const errord = result.filter(res => res.status === 'rejected')
  return {
    name: INCREMENT_TABLE,
    perf: `${Number(end - start).toFixed(2)}ms`,
    errorLength: errord.length,
    errorReason: errord[0] ? errord[0].reason.message : 'no errors'
  }
}

async function timestampUnix() {
  let func = []
  for (let i = 0; i < ROW_ADDED; i++) {
    const timeStamp = Math.floor(new Date().getTime() / 1000 + Math.random() * (ENTROPY - 1) + 1);
    const qr = db.query(`INSERT INTO ${TIMESTAMP_UNIX_TABLE} ( id, name ) VALUES ($1, $2)`, [timeStamp, 'name'])
    func.push(qr)
  }

  const start = performance.now()
  const result = await Promise.allSettled(func)
  const end = performance.now()

  const errord = result.filter(res => res.status === 'rejected')
  return {
    name: TIMESTAMP_UNIX_TABLE,
    perf: `${Number(end - start).toFixed(2)}ms`,
    errorLength: errord.length,
    errorReason: errord[0] ? errord[0].reason.message : 'no errors'
  }
}

async function uuidV4() {
  let func = []
  for (let i = 0; i < ROW_ADDED; i++) {
    const qr = db.query(`INSERT INTO ${UUID_TABLE} ( name ) VALUES ($1)`, ['name'])
    func.push(qr)
  }

  const start = performance.now()
  const result = await Promise.allSettled(func)
  const end = performance.now()

  const errord = result.filter(res => res.status === 'rejected')

  return {
    name: UUID_TABLE,
    perf: `${Number(end - start).toFixed(2)}ms`,
    errorLength: errord.length,
    errorReason: errord[0] ? errord[0].reason.message : 'no errors'
  }
}

async function incrementWithSlug() {
  let func = []
  for (let i = 0; i < ROW_ADDED; i++) {
    const randId = uuid.v4()
    const qr = db.query(`INSERT INTO ${UUID_AND_INCREMENT_TABLE} ( uuid, name ) VALUES ($1, $2)`, [randId, 'name'])
    func.push(qr)
  }

  const start = performance.now()
  const result = await Promise.allSettled(func)
  const end = performance.now()

  const errord = result.filter(res => res.status === 'rejected')
  return {
    name: UUID_AND_INCREMENT_TABLE,
    perf: `${Number(end - start).toFixed(2)}ms`,
    errorLength: errord.length,
    errorReason: errord[0] ? errord[0].reason.message : 'no errors'
  }
}

async function ulidOnly() {
  let func = []
  for (let i = 0; i < ROW_ADDED; i++) {
    const randId = ulid.ulid()
    const qr = db.query(`INSERT INTO ${ULID_TABLE} ( id, name ) VALUES ($1, $2)`, [randId, 'name'])
    func.push(qr)
  }

  const start = performance.now()
  const result = await Promise.allSettled(func)
  const end = performance.now()

  const errord = result.filter(res => res.status === 'rejected')
  return {
    name: ULID_TABLE,
    perf: `${Number(end - start).toFixed(2)}ms`,
    errorLength: errord.length,
    errorReason: errord[0] ? errord[0].reason.message : 'no errors'
  }
}

async function create() {
  console.log('---- START PROCESSING CREATE BENCHMARK ----')
  const uIncrement = await increment()
  const uTimestampUnix = await timestampUnix()
  const uUuidV4 = await uuidV4()
  const uIncrementWithSlug = await incrementWithSlug()
  const uUlid = await ulidOnly()

  console.table([uIncrement, uTimestampUnix, uUuidV4, uIncrementWithSlug, uUlid])
  console.log('---- END PROCESSING CREATE BENCHMARK ----')
}

module.exports = create
