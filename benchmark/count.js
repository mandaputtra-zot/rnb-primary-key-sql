const db = require('../db')
const uuid = require('uuid')

const {
  performance
} = require('perf_hooks')

const {
  INCREMENT_TABLE,
  TIMESTAMP_UNIX_TABLE,
  UUID_AND_INCREMENT_TABLE,
  UUID_TABLE,
  ULID_TABLE
} = require('../utils')

async function increment() {
  const start = performance.now()
  const res = await db.query(`SELECT COUNT(*) FROM ${INCREMENT_TABLE}`)
  const end = performance.now()

  return {
    name: INCREMENT_TABLE,
    rows: res.rows[0].count,
    perf: `${Number(end - start).toFixed(2)}ms`,
  }
}

async function timestampUnix() {
  const start = performance.now()
  const res = await db.query(`SELECT COUNT(*) FROM ${TIMESTAMP_UNIX_TABLE}`)
  const end = performance.now()

  return {
    name: TIMESTAMP_UNIX_TABLE,
    rows: res.rows[0].count,
    perf: `${Number(end - start).toFixed(2)}ms`,
  }
}

async function uuidV4() {
  const start = performance.now()
  const res = await db.query(`SELECT COUNT(*) FROM ${UUID_TABLE}`)
  const end = performance.now()

  return {
    name: UUID_TABLE,
    rows: res.rows[0].count,
    perf: `${Number(end - start).toFixed(2)}ms`,
  }
}

async function incrementWithSlug() {
  const start = performance.now()
  const res = await db.query(`SELECT COUNT(*) FROM ${UUID_AND_INCREMENT_TABLE}`)
  const end = performance.now()

  return {
    name: UUID_AND_INCREMENT_TABLE,
    rows: res.rows[0].count,
    perf: `${Number(end - start).toFixed(2)}ms`,
  }
}

async function ulidOnly() {
  const start = performance.now()
  const res = await db.query(`SELECT COUNT(*) FROM ${ULID_TABLE}`)
  const end = performance.now()

  return {
    name: ULID_TABLE,
    rows: res.rows[0].count,
    perf: `${Number(end - start).toFixed(2)}ms`,
  }
}

async function count() {
  console.log('---- START PROCESSING COUNT BENCHMARK ----')

  const uIncrement = await increment()
  const uTimestampUnix = await timestampUnix()
  const uUuidV4 = await uuidV4()
  const uIncrementWithSlug = await incrementWithSlug()
  const uUlid = await ulidOnly()

  console.table([uIncrement, uTimestampUnix, uUuidV4, uIncrementWithSlug, uUlid])

  console.log('---- END PROCESSING COUNT BENCHMARK ----')
}

module.exports = count
