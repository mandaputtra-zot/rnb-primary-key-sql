const db = require('./db')
const { INCREMENT_TABLE, TIMESTAMP_UNIX_TABLE, UUID_AND_INCREMENT_TABLE, UUID_TABLE, ULID_TABLE } = require('./utils')

async function dropTable() {
  // Create table for testing
  try {
    // UUID_TABLE
    await db.query(`DROP TABLE IF EXISTS ${INCREMENT_TABLE}, ${TIMESTAMP_UNIX_TABLE}, ${UUID_AND_INCREMENT_TABLE}, ${UUID_TABLE}, ${ULID_TABLE};`)
  } catch (error) {
    throw error
  }

}

module.exports = dropTable
